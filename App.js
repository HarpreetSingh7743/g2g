import React from 'react';
import {  View } from 'react-native';
import AuthAppNav from './Src/Navigators/AuthNav/AuthNav'
import SplashScreen from './Src/Screen/Splash/SplashScreen'

export default function App() {
  return (
    <View style={{flex:1}}>
      <AuthAppNav/>
    </View>
  );
}

