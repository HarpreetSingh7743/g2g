import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

import Dashboard from '../../Screen/Main/TabNavScreen/Dashboard'
import NewOrderAppNav from './NewOrderScreen_Router/New_Order'
import Cart from '../../Screen/Main/Cart'

const MainStackNav=createStackNavigator({
    Dashboard:Dashboard,
    NewOrderAppNav:NewOrderAppNav,
    Cart:Cart
    },{
        initialRouteName:"Dashboard",
        headerMode:'null'
    })
    export default createAppContainer(MainStackNav)