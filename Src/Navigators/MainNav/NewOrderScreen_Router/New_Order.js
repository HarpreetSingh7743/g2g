import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import SelectModelScreen from '../../../Screen/Main/TabNavScreen/NewOrderScreens/SelectModelScreen'
import VehicleDetails from '../../../Screen/Main/TabNavScreen/NewOrderScreens/VehicleDetails'
import SelectServices from '../../../Screen/Main/TabNavScreen/NewOrderScreens/SelectServices'
import Cart from '../../../Screen/Main/Cart'

const NewOrderAppNav=createStackNavigator({
SelectModelScreen:SelectModelScreen,
VehicleDetails:VehicleDetails,
SelectServices:SelectServices,
Cart:Cart
},{
    initialRouteName:"SelectModelScreen",
    headerMode:'null'
})
export default createAppContainer(NewOrderAppNav)