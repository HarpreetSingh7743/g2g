import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import LoginScreen from '../../Screen/Auth/LoginScreen'
import SignUpScreen from '../../Screen/Auth/SignUpScreen'
import ForgotPassword from '../../Screen/Auth/ForgotPassword'
import SideAppNav from '../SideNav/SideNavigator'

const AuthAppNav =createStackNavigator({
    SideAppNav:SideAppNav,
    SignUpScreen:SignUpScreen,
    LoginScreen:LoginScreen,
    ForgotPassword:ForgotPassword
},{
    initialRouteName:"LoginScreen",
    headerMode:'null'
});

export default createAppContainer(AuthAppNav)