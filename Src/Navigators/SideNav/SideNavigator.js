import React from "react"
import { createAppContainer } from "react-navigation";
import { createDrawerNavigator } from "react-navigation-drawer";

import CurrentOrderScreen from '../../Screen/Main/SideNavScreens/CurrentOrderScreen';
import AppNotificationScreen from '../../Screen/Main/SideNavScreens/AppNotificationScreen';
import HistoryScreen from '../../Screen/Main/SideNavScreens/HistoryScreen';
import CustomerServiceScreen from '../../Screen/Main/SideNavScreens/CustomerServiceScreen';
import AboutScreen from '../../Screen/Main/SideNavScreens/AboutScreen';

import SideNav from '../../Screen/SideNav/SideNav'
import TabNav from '../../Navigators/TabNav/TabNav'

const SideAppNav=createDrawerNavigator({
    TabNav:TabNav,
    CurrentOrderScreen:CurrentOrderScreen,
    AppNotificationScreen:AppNotificationScreen,
    HistoryScreen:HistoryScreen,
    CustomerServiceScreen:CustomerServiceScreen,
    AboutScreen:AboutScreen
},{
    initialRouteName:"TabNav",
    contentComponent:props=><SideNav {...props}/>,
    drawerPosition:'left',
    drawerWidth:'80%',
    
}); 

export default createAppContainer(SideAppNav)