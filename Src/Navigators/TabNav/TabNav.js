import {createAppContainer} from "react-navigation";
import {createBottomTabNavigator} from 'react-navigation-tabs'
import React from 'react';
import {Icon} from 'native-base'
import ProfileScreen from '../../Screen/Main/TabNavScreen/ProfileScreen'
import MainStackNav from '../MainNav/MainStackNav'
import ContactMechanicScreen from '../../Screen/Main/TabNavScreen/ContactMechanicScreen'
import UserNotificationsScreen from '../../Screen/Main/TabNavScreen/UserNotificationsScreen'
const TabNav= createBottomTabNavigator({
MainStackNav:{
    screen : MainStackNav,
    navigationOptions: {
        title:'Home',            
        tabBarIcon:({tintColor }) =>{
            return(
                <Icon name='home'
                      type='Octicons'
                      style={{
                          color:tintColor,
                          fontSize : 20
                      }} />
                      )
                    }
  }
},
ContactMechanicScreen:{
    screen : ContactMechanicScreen,
    navigationOptions: {
        title:'Mechanic',            
        tabBarIcon:({ tintColor }) =>{
            return(
                <Icon name='tool'
                      type='AntDesign'
                      style={{
                          color:tintColor,
                          fontSize : 20
                      }} />
                      )
                    }
  }
},
UserNotificationsScreen:{
    screen : UserNotificationsScreen,
    navigationOptions: {
        title:'Notifications',            
        tabBarIcon:({tintColor }) =>{
            return(
                <Icon name='ios-notifications-outline'
                      type='Ionicons'
                      style={{
                          color:tintColor,
                          fontSize : 20
                      }} />
                      )
                    }
  }
},
ProfileScreen:{
    screen : ProfileScreen,
    navigationOptions: {
        title:'Profile',            
        tabBarIcon:({ tintColor }) =>{
            return(
                <Icon name='user'
                      type='FontAwesome5'
                      style={{
                          color:tintColor,
                          fontSize : 20,
                      }} />
                      )
                    }
  }
},
},

        {
        initialRouteName:"MainStackNav",
        tabBarOptions:{
            activeTintColor:'#fff',
            inactiveTintColor:'#000',
            inactiveBackgroundColor:'#f11',
            activeBackgroundColor:'#f11'
        }
        
});
export default createAppContainer(TabNav);
