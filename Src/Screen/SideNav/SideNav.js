import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Icon } from 'native-base'
import UserProfile from './UserProfile.js'
class SideNav extends Component {
    state={
        userName:"Harpreet Singh",
        email:"citizen.harpreetpanks@gmail.com",
        login:true
    }

    checkLogin(){
        if(this.state.login==true)
        {return(<View style={{margin:responsiveHeight(3),flex:1,flexDirection:'row',alignItems:'center'}}>
        <View style={{margin:5,width:responsiveWidth(11),height:responsiveHeight(11)}}>
        <UserProfile/>
        </View>
     <View style={{marginLeft:responsiveHeight(2),margin:responsiveHeight(1)}}>
        <Text style={{color:'#fff',fontSize:responsiveFontSize(2),fontWeight:'bold'}}>{this.state.userName}</Text>
        <Text style={{color:'#fff',fontSize:responsiveFontSize(1.2)}}>{this.state.email}</Text>
     </View>
    </View>)}
        else if(this.state.login==false)
        {return(<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity style={{borderColor:'#fff',borderWidth:1,borderRadius:20,padding:20}}>
                        <Text style={{color:'#fff',fontSize:responsiveFontSize(2)}}>SignIn Here</Text>
                    </TouchableOpacity>
                </View>)}
    }

    render() {
        return (
            <View style={{flex:1,backgroundColor:'#000'}}>
                <View style={{flex:1,marginTop:responsiveHeight(3)}}>
                <View style={styles.customHeader}>
                    {this.checkLogin()}
                </View>
                <View style={styles.customFooter}>
                    <View style={{margin:responsiveWidth(4)}}>
                        <TouchableOpacity style={styles.customButton}
                        onPress={()=>{this.props.navigation.navigate("MainStackNav")}}>
                            <Icon name="home" style={styles.customIcon}/>
                            <Text style={styles.customtext}> Home</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{margin:responsiveWidth(4)}}>
                        <TouchableOpacity style={styles.customButton}
                        onPress={()=>{this.props.navigation.navigate("CurrentOrderScreen")}}>
                        <Icon name="book-open" type="SimpleLineIcons" style={styles.customIcon}/>
                         <Text style={styles.customtext}>Current Orders</Text>
                        </TouchableOpacity>
                    </View>
                    
                    <View style={{margin:responsiveWidth(4)}}>
                        <TouchableOpacity style={styles.customButton}
                        onPress={()=>{this.props.navigation.navigate("AppNotificationScreen")}}>
                        <Icon name="ios-notifications-outline" type="Ionicons" style={styles.customIcon}/>
                        <Text style={styles.customtext}>  App Notifications</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{margin:responsiveWidth(4)}}>
                        <TouchableOpacity style={styles.customButton}
                         onPress={()=>{this.props.navigation.navigate("HistoryScreen")}}>
                        <Icon name="history" type="MaterialCommunityIcons" style={styles.customIcon}/>
                         <Text style={styles.customtext}>History</Text>
                        </TouchableOpacity>
                    </View>


                    <View style={{margin:responsiveWidth(4)}}>
                        <TouchableOpacity style={styles.customButton}
                         onPress={()=>{this.props.navigation.navigate("CustomerServiceScreen")}}>
                        <Icon name="user-female" type="SimpleLineIcons" style={styles.customIcon}/>
                        <Text style={styles.customtext}>Customer Service</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{margin:responsiveWidth(4)}}>
                        <TouchableOpacity style={styles.customButton}
                         onPress={()=>{this.props.navigation.navigate("AboutScreen")}}>
                        <Icon name="question" type="SimpleLineIcons" style={styles.customIcon}/>
                        <Text style={styles.customtext}>About</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                </View>    
            </View>
        )
    }
}

export default SideNav
const styles=StyleSheet.create({
    customHeader:{
        flex:0.2,
    },
    customFooter:{
        flex:0.8,
    },
    customIcon:{
        color:'#fff',
        marginRight:responsiveWidth(2)
    },
    customtext:{
        color:'#fff',
        marginLeft:responsiveWidth(2),
        fontSize:responsiveFontSize(2)
    },
    customButton:{
        flexDirection:'row',
        alignItems:'center'
    }
});
