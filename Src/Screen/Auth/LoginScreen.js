import React, { Component } from 'react'
import { Text, View,StyleSheet, ImageBackground, TextInput, KeyboardAvoidingView } from 'react-native'
import { responsiveHeight, responsiveFontSize, responsiveWidth } from 'react-native-responsive-dimensions';
import { Icon } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';

 class LoginScreen extends Component {
    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="position">
                <ImageBackground
                source={require('../../../assets/Images/loginbg.png')}
                style={{height:responsiveHeight(105),width:responsiveWidth(100)}}>
                    <SafeAreaView style={{flex:0.4}}>

                    </SafeAreaView>
                    <SafeAreaView style={{flex:0.4}}>
                        <Text style={{fontSize:responsiveFontSize(6),color:'#f11',fontWeight:'bold'}}>Login</Text>
                        <SafeAreaView style={{alignItems:'center',justifyContent:'center',flex:0.6}}>
                            <SafeAreaView style={{flexDirection:'row',alignItems:'center'}}>
                                <Icon name="mail" type="AntDesign"/>
                                <TextInput style={styles.customTextInput}
                                placeholder="Enter Email"/>
                            </SafeAreaView>
                            <SafeAreaView style={{flexDirection:'row',alignItems:'center'}}>
                                <Icon name="lock" type="SimpleLineIcons"/>
                                <TextInput style={styles.customTextInput} 
                                placeholder="Enter Password"/>
                            </SafeAreaView>
                        </SafeAreaView>
                        <SafeAreaView style={{flexDirection:'row',flex:0.4}}>
                        <SafeAreaView style={{flex:0.6}}>

                        </SafeAreaView>
                        <SafeAreaView style={{flex:0.4}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("ForgotPassword")}}>
                            <Text style={{color:'#f11'}}>Forgot Password ?</Text>
                            </TouchableOpacity>
                        </SafeAreaView>
                        </SafeAreaView>
                    </SafeAreaView>
                    <SafeAreaView style={{flex:0.23,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                        <TouchableOpacity style={styles.customButton1}
                        onPress={()=>{this.props.navigation.navigate("SideAppNav")}}>
                            <Text style={styles.customButton1Text}>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.customButton2}
                        onPress={()=>{this.props.navigation.navigate("SignUpScreen")}}>
                            <Text style={styles.customButton2Text}>SignUp</Text>
                        </TouchableOpacity>
                    </SafeAreaView>
                </ImageBackground>
            </KeyboardAvoidingView>
        )
    }
}

export default LoginScreen

const styles=StyleSheet.create({
    customTextInput:{
        borderBottomWidth:1,
        borderBottomColor:'#f11',
        width:responsiveWidth(70),
        marginVertical:responsiveHeight(1.1),
        marginHorizontal:responsiveWidth(1)
    },
    customButton1:{
        height:responsiveHeight(7),
        width:responsiveWidth(30),
        backgroundColor:'#fff',
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:responsiveWidth(4),
        borderRadius:30

    },
    customButton2:{
        height:responsiveHeight(7),
        width:responsiveWidth(30),
        alignItems:'center',
        justifyContent:'center',
        borderWidth:1,
        borderColor:'#fff',
        marginHorizontal:responsiveWidth(4),
        borderRadius:30
    },
    customButton1Text:{
        color:'#f11'
    },
    customButton2Text:{
        color:'#fff'
    }
});