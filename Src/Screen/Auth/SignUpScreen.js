import React, { Component } from 'react'
import { Text, View, KeyboardAvoidingView, ImageBackground,Image,TextInput,StyleSheet,TouchableOpacity } from 'react-native'
import { responsiveHeight,responsiveWidth,responsiveFontSize } from "react-native-responsive-dimensions"
import { SafeAreaView } from 'react-navigation'
import { Icon,CheckBox } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
class SignUpScreen extends Component {
    state={
        check:false,
        UserName:"",
        Mobile:"",
        Email:"",
        Password:"",
        CPassword:""
    }
    validUname(){
        let Uname=this.state.UserName
       
        if (Uname.length>=3)
        {return(<Image source={require('../../../assets/Images/Icons/Markers/correct.jpg')}
            style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}

        else {return(<Image source={require('../../../assets/Images/Icons/Markers/incorrect.jpg')}
        style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}
    }
    validMobile(){
        let Mob=this.state.Mobile
        var IndNum = /^[0]?[6789]\d{9}$/;

        if (Mob.length==10 && Mob.match(IndNum))
        {return(<Image source={require('../../../assets/Images/Icons/Markers/correct.jpg')}
            style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}

        else {return(<Image source={require('../../../assets/Images/Icons/Markers/incorrect.jpg')}
        style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}
    }
    validEmail(){
        let mail=this.state.Email
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (mail.match(mailformat))
        {
             return(<Image source={require('../../../assets/Images/Icons/Markers/correct.jpg')}
             style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}
             
        else {return(<Image source={require('../../../assets/Images/Icons/Markers/incorrect.jpg')}
        style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}
        }
    validPassword(){
        let pass=this.state.Password
        var passw=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
        if (pass.match(passw))
        {return(<Image source={require('../../../assets/Images/Icons/Markers/correct.jpg')}
            style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}

        else {return(<Image source={require('../../../assets/Images/Icons/Markers/incorrect.jpg')}
        style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}
    }
    validCPassword(){
        let Cpass=this.state.CPassword
        let pass=this.state.Password
        if (Cpass===pass && Cpass.length>=8)
        {return(<Image source={require('../../../assets/Images/Icons/Markers/correct.jpg')}
            style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}

        else {return(<Image source={require('../../../assets/Images/Icons/Markers/incorrect.jpg')}
        style={{width:responsiveWidth(6),height:responsiveHeight(3.1)}}/>)}
    }

    render() {
        return (
            <SafeAreaView style={{flex:1}}>
                <ScrollView style={{flex:1}}>
                <ImageBackground
                source={require('../../../assets/Images/authbg.png')}
                style={{height:responsiveHeight(100),width:"100%"}}>
                    <View style={{flex:0.15,padding:responsiveWidth(4)}}>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(5),fontWeight:'bold', marginTop:10}}>New user ?</Text>
                    </View>
                    <View style={{flex:0.7,padding:responsiveHeight(3),justifyContent:'center',alignItems:'center'}}>
                        <View style={styles.customViews}>
                        <Icon name="user" type="SimpleLineIcons" style={{color:'#f11'}}/>
                            <TextInput style={styles.customTextInput}
                            placeholder="Enter Full Name"
                            onChangeText={(UserName)=>{this.setState({UserName})}}></TextInput>
                            <View style={{width:responsiveWidth(6.5),height:responsiveHeight(3.5)}}>
                                {this.validUname()}
                            </View>
                        </View>
                        <View style={styles.customViews}>
                        <Icon name="phone" type="SimpleLineIcons" style={{color:'#f11'}}/>
                            <TextInput style={styles.customTextInput}
                            placeholder="Enter Mobile"onChangeText={(Mobile)=>{this.setState({Mobile})}}></TextInput>
                            <View style={{width:responsiveWidth(6.5),height:responsiveHeight(3.5)}}>
                                {this.validMobile()}
                            </View>
                        </View>
                        <View style={styles.customViews}>
                        <Icon name="mail" type="AntDesign" style={{color:'#f11'}}/>
                            <TextInput style={styles.customTextInput}
                            placeholder="Enter Email"onChangeText={(Email)=>{this.setState({Email})}}></TextInput>
                            <View style={{width:responsiveWidth(6.5),height:responsiveHeight(3.5)}}>
                                {this.validEmail()}
                            </View>
                        </View>
                        <View style={styles.customViews}>
                        <Icon name="lock" type="SimpleLineIcons" style={{color:'#f11'}}/>
                            <TextInput style={styles.customTextInput}
                            placeholder="Enter Password."
                            secureTextEntry={true}onChangeText={(Password)=>{this.setState({Password})}}></TextInput>
                            <View style={{width:responsiveWidth(6.5),height:responsiveHeight(3.5)}}>
                                {this.validPassword()}
                            </View>
                        </View>
                        <View style={styles.customViews}>
                        <Icon name="lock" type="SimpleLineIcons" style={{color:'#f11'}}/>
                            <TextInput style={styles.customTextInput}
                            placeholder="Confirm Password."
                            secureTextEntry={true}onChangeText={(CPassword)=>{this.setState({CPassword})}}></TextInput>
                            <View style={{width:responsiveWidth(6.5),height:responsiveHeight(3.5)}}>
                                {this.validCPassword()}
                            </View>
                        </View>
                        
                        <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{flexDirection:'row'}}
                                onPress={()=>{this.setState({'check':!(this.state.check)})}}>
                                <CheckBox color="#f11" 
                                onPress={()=>{this.setState({'check':!(this.state.check)})}}
                                checked={this.state.check}/>
                                <Text style={{marginLeft:responsiveHeight(2),color:'#f11'}}>   I hereby confirm to be 18 years old        </Text>
                                </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{flex:0.15,padding:responsiveWidth(2),marginTop:20,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity onPress={()=>{alert(this.state.UserName)}}>
                                <View style={styles.customButton1}>
                                    <Text style={styles.customButton1Text}>Register</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("LoginScreen")}}>
                                <View style={styles.customButton2}>
                                    <Text style={styles.customButton2Text}>Login</Text>
                                </View>
                            </TouchableOpacity>
                    </View>
                </ImageBackground>
                </ScrollView>
                </SafeAreaView>
        )
    }
}

export default SignUpScreen

const styles=StyleSheet.create({
    customButton1:{
        height:responsiveHeight(7),
        width:responsiveWidth(30),
        backgroundColor:'#fff',
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:responsiveWidth(4),
        borderRadius:30

    },
    customButton2:{
        height:responsiveHeight(7),
        width:responsiveWidth(30),
        alignItems:'center',
        justifyContent:'center',
        borderWidth:1,
        borderColor:'#fff',
        marginHorizontal:responsiveWidth(4),
        borderRadius:30
    },
    customButton1Text:{
        color:'#f11'
    },
    customButton2Text:{
        color:'#fff'
    },
    customTextInput:{
        borderBottomWidth:1,
        borderBottomColor:'#f11',
        width:responsiveWidth(60),
        marginVertical:responsiveHeight(4),
        marginHorizontal:responsiveWidth(1)
    },
    customViews:{
        flexDirection:'row',
        alignItems:'center'
    }
})