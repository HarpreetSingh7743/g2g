import React, { Component } from 'react'
import { Text, View,ImageBackground,TouchableOpacity,StyleSheet, TextInput, KeyboardAvoidingView } from 'react-native'
import { responsiveHeight,responsiveFontSize,responsiveWidth } from 'react-native-responsive-dimensions'
import { Icon } from 'native-base'
import { SafeAreaView } from 'react-navigation'

 class ForgotPassword extends Component {
     state={
         currentScreen:"OTP",
         buttonText:"Get OTP",
         Email:""
     }

     renderScreen(){
         if(this.state.currentScreen=="OTP")
         return(
             <SafeAreaView style={{alignItems:'center',justifyContent:'center'}}>
                <SafeAreaView style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <Icon name="mail" type="AntDesign" style={{color:'#f11'}}/>
                    <TextInput style={styles.customTextInput}
                    placeholder="Enter Email"
                    onChangeText={(Email)=>{this.setState({Email:Email})}}/>
                </SafeAreaView>
             </SafeAreaView>
         )
         else if(this.state.currentScreen=="Reset"){
             return(
                 <SafeAreaView style={{alignItems:'center',justifyContent:'center'}}>
                    <SafeAreaView style={{flexDirection:'row',alignItems:'center'}}>
                        <Icon name="pencil" type="Entypo" style={{color:'#f11'}}/>
                        <TextInput style={styles.customTextInput}
                         placeholder="Enter OTP"/>
                    </SafeAreaView>
                    <SafeAreaView style={{flexDirection:'row',alignItems:'center'}}>
                    <Icon name="lock" type="SimpleLineIcons" style={{color:'#f11'}}/>
                        <TextInput style={styles.customTextInput}
                         placeholder="Enter New Password"/>
                    </SafeAreaView>
                    <SafeAreaView style={{flexDirection:'row',alignItems:'center'}}>
                    <Icon name="lock" type="SimpleLineIcons" style={{color:'#f11'}}/>
                        <TextInput style={styles.customTextInput}
                         placeholder="Confirm New Password"/>
                    </SafeAreaView>
                 </SafeAreaView>
             )
         }
         else{
             alert("Error Loading Please restart the application!")
         }
     }

    render() {
        return (
                <KeyboardAvoidingView style={{flex:1}} behavior="position">
                    <ImageBackground
                     source={require('../../../assets/Images/loginbg.png')}
                     style={{height:responsiveHeight(105),width:responsiveWidth(100)}}>
                        <SafeAreaView style={{flex:0.45}}>

                        </SafeAreaView>
                        <SafeAreaView style={{flex:0.4}}>
                            <SafeAreaView style={{flex:0.1}}>
                        <Text style={{color:'#f11',fontSize:responsiveFontSize(4),fontWeight:'bold'}}>Recover Password?</Text>
                            </SafeAreaView>
                        <SafeAreaView style={{marginTop:responsiveWidth(5),flex:0.9,alignItems:'center',justifyContent:'center'}}>
                            {this.renderScreen()}
                        </SafeAreaView>
                        </SafeAreaView>
                        <SafeAreaView style={{flex:0.23}}>
                            <SafeAreaView style={{flex:1,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                <TouchableOpacity style={styles.customButton1}
                                onPress={()=>{let mail=this.state.Email
                                              var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                              if(mail.match(mailformat))
                                              {
                                                this.setState({currentScreen:'Reset',buttonText:'Reset Password'})
                                              }
                                              else{
                                                  alert("Please enter Valid email")
                                              }
                                              }}>
                                    <Text style={{color:'#f11'}}>{this.state.buttonText}</Text>
                                </TouchableOpacity>
                            </SafeAreaView>
                        </SafeAreaView>
                    </ImageBackground>
                </KeyboardAvoidingView>
        )
    }
}

export default ForgotPassword

const styles=StyleSheet.create({
    customButton1:{
        height:responsiveHeight(7),
        width:responsiveWidth(50),
        backgroundColor:'#fff',
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:responsiveWidth(4),
        borderRadius:30

    },
    customButton1Text:{
        color:'#f11'
    },
    customTextInput:{
        borderBottomWidth:1,
        borderBottomColor:'#f11',
        width:responsiveWidth(70),
        marginVertical:responsiveHeight(2),
        marginHorizontal:responsiveWidth(1)
    },
})