import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { responsiveFontSize,responsiveHeight,responsiveWidth } from "react-native-responsive-dimensions";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'native-base';
class Cart extends Component {
    
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:0.15,backgroundColor:'#f11'}}>
                    <View style={{marginTop:responsiveHeight(3),flex:1,justifyContent:'center',
                                    alignItems:'center',marginHorizontal:responsiveWidth(3),flexDirection:'row'}}>
                        <View style={{flex:0.7}}>
                            <TouchableOpacity style={{flexDirection:'row'}}
                            onPress={()=>{this.props.navigation.goBack()}}>
                            <Icon name="md-arrow-round-back" type="Ionicons" style={{color:'#fff'}}/>
                            <Text style={{color:'#fff',marginHorizontal:5,fontSize:responsiveFontSize(2)}}>Back</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:0.3}}>
                            <TouchableOpacity style={{flexDirection:'row'}}>
                                <Text style={{color:'#fff',marginHorizontal:5,fontSize:responsiveFontSize(2)}}>Checkout</Text>
                                <Icon name="ios-arrow-forward" type="Ionicons" style={{color:'#fff'}}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{flex:0.79}}></View>

                <View style={{flex:0.05,backgroundColor:'#f11',borderRadius:20,alignItems:'center',marginHorizontal:responsiveWidth(3),
                              justifyContent:'center',flexDirection:'row'}}>
                    <TouchableOpacity style={{flex:1,width:responsiveWidth(90),height:responsiveHeight(2),alignItems:"center",justifyContent:'center'}}>
                        <Text style={{color:'#fff'}}>Clear Cart</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default Cart

