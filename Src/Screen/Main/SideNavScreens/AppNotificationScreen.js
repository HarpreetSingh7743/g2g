import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Icon } from 'native-base'

 class AppNotificationScreen extends Component {
    

    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:0.15,backgroundColor:'#f11',justifyContent:'center'}}>
                    <View style={{marginVertical:responsiveWidth(15),flexDirection:'row',paddingHorizontal:10,alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>{this.props.navigation.openDrawer()}}>
                        <Icon name="menu" type="Feather" style={{color:'#fff'}}/>
                        </TouchableOpacity>
                        <Text style={{color:'#fff',fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(2)}}>
                            Notifications
                        </Text>
                    </View>

                </View>
                <View style={{flex:0.85,justifyContent:'center',alignItems:'center'}}>
                    <View>
                    <Text>
                        New Notifications
                    </Text>
                    </View>

                </View>
            </View>
        )
    }
}

export default AppNotificationScreen
