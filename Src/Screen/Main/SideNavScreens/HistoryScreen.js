import React, { Component } from 'react'
import { Text, View,TouchableOpacity,StyleSheet, ScrollView } from 'react-native'
import { responsiveHeight,responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Icon,Card,CardItem } from 'native-base'

 class HistoryScreen extends Component {
    arrayView=[]
    numb=""
    arrayContent=[
        ["Bajaj V15","2015","CH01 BJ 9656","CH Customs","#81,Industrial Area Phase 1","Mukesh Kumar","1 Day","9501205790",],
        ["Honda Amaze","2005","CH 08 BN 4986","Singh Motors","#441,Industrial Area Phase 3","Sandeep Kumar","7 Days","9876543210"],
        ["Bajaj Dominar","2009","PB 98 KN 8897","Bajaj Service Center","Sector 31,Chandigarh","Santosh Kumar","31 days","9090989887"],
        ["Tata Nexon","2012","CH 06 AN","Tata Care","Phase 6, Mohali","Dinesh","90 days","9898865460"]
    ]
    state={
        numberOfNotifications:this.arrayContent.length
    }
    drawView=()=>{
        this.arrayView=[];
        for(let i=0;i<this.state.numberOfNotifications;i++)
        {
            this.arrayView.push(
                <Card style={styles.notificationView} key={i}>
                            <CardItem style={{backgroundColor:'#f11',borderRadius:10}}>
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <View style={{flex:0.7}}>
                                    <Text style={{color:'#fff',fontSize:responsiveFontSize(1.5),fontWeight:'bold'}}>{i+1}. {this.arrayContent[i][0]} {this.arrayContent[i][1]}</Text>
                                    </View>
                                    <View style={{flex:0.3,justifyContent:'center'}}>
                                    <Text style={{color:'#fff',fontSize:responsiveFontSize(1.30)}}>{this.arrayContent[i][6]} ago</Text>
                                    </View>

                                </View>
                                </CardItem>
                            <View style={{flex:0.7,padding:responsiveWidth(2),width:responsiveWidth(80)}}>
                            <View style={{flex:1,padding:responsiveWidth(1.5)}}>
                                <Text>Reg No. : {this.arrayContent[i][2]}</Text>
                                <Text>Garage : {this.arrayContent[i][3]}</Text>
                                <Text>Location : {this.arrayContent[i][4]}</Text>
                                <Text>Mechanic : {this.arrayContent[i][5]}</Text>
                            </View>
                            </View>
                            <View  style={{flex:0.3,flexDirection:'row'}}>
                            <TouchableOpacity style={styles.notificationButtons}
                                                    onPress={() => {alert("Going to Garage")}}>
                                    <Text>Get Directions</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.notificationButtons}
                                                    onPress={() => {alert("please rate")}}>
                                    <Text>Rate Garage</Text>
                                </TouchableOpacity>
                            </View>
                </Card>
            )
        }
    }
    render() {
        this.drawView()
        return (
            <View style={{flex:1}}>
                <View style={{flex:0.15,backgroundColor:'#f11',justifyContent:'center'}}>
                    <View style={{marginVertical:responsiveWidth(15),flexDirection:'row',paddingHorizontal:10,alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>{this.props.navigation.openDrawer()}}>
                        <Icon name="menu" type="Feather" style={{color:'#fff'}}/>
                        </TouchableOpacity>
                        <Text style={{color:'#fff',fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(2)}}>
                            History
                        </Text>
                    </View>

                </View>
                <View style={{flex:0.85,justifyContent:'center',alignItems:'center'}}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.arrayView}
                    </ScrollView>

                </View>
            </View>
        )
    }
}

export default HistoryScreen


const styles=StyleSheet.create({
    notificationView:{
        width:responsiveWidth(85),
        height:responsiveHeight(28),
        borderWidth:3,
        marginVertical:responsiveHeight(1.5),
        borderRadius:10,
        alignItems:'center'
    },
    notificationButtons:{
        borderWidth:3,
        width:responsiveWidth(30),
        height:responsiveHeight(4),
        alignItems:'center',
        justifyContent:'center',
        borderColor:'#f11',
        borderRadius:10,
        marginHorizontal:responsiveWidth(3)
    }
});