import React, { Component } from 'react'
import { Text, View, ImageBackground,StyleSheet, TouchableOpacity,Linking, StatusBar } from 'react-native'
import { responsiveHeight, responsiveFontSize, responsiveWidth } from 'react-native-responsive-dimensions'
import { Accordion, Icon } from 'native-base';
import {SafeAreaView} from "react-native-safe-area-context"

const dataArray = [
    { title: "Terms and Conditions", content: "Nothing to write because this app is in developement phase" },
    { title: "About Team", content: "Names will be added soon !" },
  ];

 class AboutScreen extends Component {
     state={
         appVersion:'0.0.1'
     }
     
    render() {
        return (
            <SafeAreaView style={{flex:1}}>
            <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#6b0101" translucent = {true}/>
            <ImageBackground
            source={require('../../../../assets/Images/op.png')} 
            style={{flex:1}}>
                    <View style={{flex:0.85,margin:responsiveHeight(3)}}>
                        <View style={{marginVertical:responsiveHeight(10),alignItems:'center',justifyContent:'center'}}> 
                            <Text style={{fontSize:responsiveFontSize(4),fontWeight:'bold',color:'#f11'}}>
                                GO 2 GARAGE
                            </Text>
                            <Text>Version  :  {this.state.appVersion}</Text>
                        </View>
                        
                        <View>
                        <Accordion dataArray={dataArray}/>
                        </View>
                    </View>
                    <View style={{flex:0.1,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <TouchableOpacity style={styles.footerButtons}
                             onPress={ ()=>{ Linking.openURL('https://facebook.com')}}>
                            <Icon name="social-facebook" type="SimpleLineIcons" style={styles.footerIcons}/>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.footerButtons}
                             onPress={ ()=>{ Linking.openURL('https://Instagram.com')}}>
                            <Icon name="social-instagram" type="SimpleLineIcons" style={styles.footerIcons}/>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.footerButtons}
                             onPress={ ()=>{ Linking.openURL('https://twitter.com')}}>
                            <Icon name="social-twitter" type="SimpleLineIcons" style={styles.footerIcons}/>
                            </TouchableOpacity>
                    </View>
                    <View style={{flex:0.05,alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity onPress={()=>{Linking.openURL('https://play.google.com/store/apps/details?id=com.google.android.apps.nbu.paisa.user')}}>
                        <Text style={{color:'#fff'}}> Own a Garage? Tap to Register</Text>
                        </TouchableOpacity>
                    </View>
            </ImageBackground>
            </SafeAreaView>
        )
    }
}

export default AboutScreen

const styles=StyleSheet.create({
    footerIcons:{
        color:'#fff'
    },
    footerButtons:{
        marginHorizontal:responsiveWidth(8),
        padding:7,
        borderColor:'#fff',
    }
});
