import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Button, Card,Icon} from 'native-base';
import {responsiveWidth, responsiveHeight, responsiveFontSize} from 'react-native-responsive-dimensions';
import { Dropdown } from 'react-native-material-dropdown';
import data1 from './Cars/Car_Brands/MarutiSuzuki/index';
import data2 from './Cars/Car_Brands/Hyundai/index';
import data3 from './Cars/Car_Brands/Honda/index';
import data4 from './Cars/Car_Brands/Mahindra/index';
import data5 from './Cars/Car_Brands/Tata/index';
import data6 from './Cars/Car_Brands/Ford/index';
import data7 from './Cars/Car_Brands/Renault/index';
import data8 from './Cars/Car_Brands/Nissan/index';
import data9 from './Cars/Car_Brands/Toyota/index';
import hd from './Bikes/BikesModels/Harley Davidson/index';
import heromotocorp from './Bikes/BikesModels/Hero Moto Corp/index';
import honda from './Bikes/BikesModels/Honda/index';
import ktm from './Bikes/BikesModels/KTM/index';
import kawasaki from './Bikes/BikesModels/Kawasaki/index';
import mahindra from './Bikes/BikesModels/Mahindra/index';
import royal from './Bikes/BikesModels/RoyalEnfield/index';
import suzuki from './Bikes/BikesModels/Suzuki/index';
import tvs from './Bikes/BikesModels/TVS/index';
import vespa from './Bikes/BikesModels/Vespa/index';
import yamaha from './Bikes/BikesModels/Yamaha/index';

let data = [{value:'Hyundai'}, 
    {value: 'Maruti'},
    {value: 'Honda'},
    {value: 'Tata'},
    {value: 'Mahindra'},
    {value: 'Renault'},
    {value: 'Ford'},
    {value: 'Nissan'},
    {value: 'Toyota'},
];

let bikecompanies = [{value:'Hero Moto Corp'},
    {value:'Honda'},
    {value:'Yamaha'},
    {value:'TVS'},
    {value:'KTM'},
    {value:'Suzuki'},
    {value:'Mahindra'},
    {value:'Kawasaki'},
    {value:'Vespa'},
    {value:'Harley Davidson'},
    {value:'Royal Enfield'}
];

let models = [];
let companies = [];
var vehicle = [{brand:''},
    {model:''},
    {type:''}
];

export default class SelectModelScreen extends Component{
    state={
        brand : '',
        model : ''
    }

    constructor(props){
        super(props);
        this.state = {
            selected: this.props.navigation.state.params.selected,
          };
    }

    render(){
        const { navigate } = this.props.navigation
        vehicle[2] = this.state.selected;
        vehicle[1] = this.state.model;
        vehicle[0] = this.state.brand;
        
        if(this.state.selected === 'car')
        {
            this.companies = data;
            if(this.state.brand === 'Maruti'){
                this.models = data1
            }
            if (this.state.brand === 'Hyundai') {
            this.models = data2  
            }
            if (this.state.brand === 'Honda') {
                this.models = data3  
            }
            if (this.state.brand === 'Mahindra') {
                this.models = data4  
            }
            if (this.state.brand === 'Tata') {
                this.models = data5  
            }
            if (this.state.brand === 'Ford') {
                this.models = data6 
            }
            if (this.state.brand === 'Renault') {
                this.models = data7  
            }
            if (this.state.brand === 'Nissan') {
                this.models = data8  
            }
            if (this.state.brand === 'Toyota') {
                this.models = data9  
            }
        }

        if(this.state.selected === 'bike')
        {
            this.companies = bikecompanies;
            if(this.state.brand === 'Hero Moto Corp'){
                this.models = heromotocorp
            }
            if (this.state.brand === 'Honda') {
            this.models = honda  
            }
            if (this.state.brand === 'Yamaha') {
                this.models = yamaha  
            }
            if (this.state.brand === 'TVS') {
                this.models = tvs  
            }
            if (this.state.brand === 'Suzuki') {
                this.models = suzuki  
            }
            if (this.state.brand === 'Harley Davidson') {
                this.models = hd 
            }
            if (this.state.brand === 'Vespa') {
                this.models = vespa  
            }
            if (this.state.brand === 'KTM') {
                this.models = ktm  
            }
            if (this.state.brand === 'Royal Enfield') {
                this.models = royal  
            }
            if(this.state.brand === 'Mahindra'){
                this.models = mahindra
            }
            if(this.state.brand === 'Kawasaki'){
                this.models = kawasaki
            }   
        }
        
        return(
            <View style={{flex:1}}>
                <View style={{flex:0.2,backgroundColor:'#f11'}}>
                <View style={{marginTop:responsiveHeight(6),justifyContent:'center'}}>
                        <View style={{flexDirection:'row',marginHorizontal:responsiveHeight(2)}}>
                           <View style={{flex:0.9}}>
                           <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}}
                           onPress={()=>{this.props.navigation.navigate("Dashboard")}}>
                                <Icon name="md-arrow-round-back" type="Ionicons" style={{color:'#fff'}}/>
                                <Text style={{color:'#fff',marginHorizontal:5,fontSize:responsiveFontSize(2)}}>Back</Text>
                            </TouchableOpacity>
                           </View>
                           <View style={{flex:0.1}}>
                               <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Cart")}}>
                                   <Icon name="shopping-cart" type="Feather" style={{color:'#fff'}}/>
                               </TouchableOpacity>
                           </View>
                            
                        </View>
                    </View>
                </View>
                <View style={{flex:0.6}}>
                        <View style={styles.customVeiw}>
                            <Text style={styles.headerText}>SELECT YOUR VEHICLE</Text>
                        </View>
                        <View style={{flex:0.7,alignItems:'center'}}>
                            <Card style={{padding:responsiveHeight(4)}}>
                            <View style={styles.customDropdown}>
                                <Dropdown
                                    label='SELECT BRAND'
                                    data={this.companies}
                                    containerStyle={styles.container}
                                    fontSize={responsiveFontSize(1.5)}
                                    onChangeText={(input)=>this.setState({brand:input})}/>
                            </View>
                            <View style={styles.customDropdown}>
                                <Dropdown
                                    label='SELECT MODEL'
                                    data={this.models}
                                    containerStyle={styles.container}
                                    fontSize={responsiveFontSize(1.5)}
                                    onChangeText={(input)=>this.setState({model:input})}/>
                            </View>
                            <TouchableOpacity style={{alignItems:'center'}}>
                                <Button style={styles.submitButton}
                                onPress={()=>{
                                    if( vehicle[0]==undefined || vehicle[1]==undefined)
                                    {
                                        alert("Please fill Details first.")
                                    }
                                    else{
                                        navigate('VehicleDetails',{vehicle:vehicle})
                                    }
                                    }}>
                                    <Text style={{color:'#fff',fontWeight:'bold'}}>Submit</Text>
                                </Button>
                            </TouchableOpacity>
                            </Card>
                        </View>
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerText:{
        fontSize:responsiveFontSize(3),
        fontWeight:'bold',
        color:'#f11'
    },
    customVeiw:{
        flex:0.3,
        justifyContent:'center',
        padding:10
    },
    submitButton:{
        width:responsiveWidth(40),
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#f11',
        borderRadius:20
    },
    customDropdown:{
        width:responsiveWidth(70),
        marginVertical:responsiveHeight(3)
    }
});
