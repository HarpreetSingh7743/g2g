import React, { Component } from 'react'
import { Text, View, TouchableOpacity,StyleSheet } from 'react-native'
import { responsiveWidth, responsiveHeight,responsiveFontSize } from 'react-native-responsive-dimensions'
import { Icon, Card, CardItem } from 'native-base'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import RoundCheckbox from 'rn-round-checkbox';

 class Bike_All_Services extends Component {

  constructor(props){
    super(props);
    this.state={
      "isSelected1":false,
      "isSelected2":false,
      "isSelected3":false,
      "isSelected4":false,
      "isSelected5":false,
      "isSelected6":false,
      "isSelected7":false,
      "isSelected8":false,
      "isSelected9":false,
      "isSelected10":false,
      "isSelected11":false,
      "isSelected12":false,
      visible: false
    }
  }
  checkboxTest1()
  {
    this.setState({
      isSelected1:!this.state.isSelected1
    })
  }
  checkboxTest2()
  {
    this.setState({
      isSelected2:!this.state.isSelected2
    })
  }
  checkboxTest3()
  {
    this.setState({
      isSelected3:!this.state.isSelected3
    })
  }
  checkboxTest4()
  {
    this.setState({
      isSelected4:!this.state.isSelected4
    })
  }
  checkboxTest5()
  {
    this.setState({
      isSelected5:!this.state.isSelected5
    })
  }
  checkboxTest6()
  {
    this.setState({
      isSelected6:!this.state.isSelected6
    })
  }
  checkboxTest7()
  {
    this.setState({
      isSelected7:!this.state.isSelected7
    })
  }
  checkboxTest8()
  {
    this.setState({
      isSelected8:!this.state.isSelected8
    })
  }
  checkboxTest9()
  {
    this.setState({
      isSelected9:!this.state.isSelected9
    })
  }
  checkboxTest10()
  {
    this.setState({
      isSelected10:!this.state.isSelected10
    })
  }
  checkboxTest11()
  {
    this.setState({
      isSelected11:!this.state.isSelected11
    })
  }
  checkboxTest12()
  {
    this.setState({
      isSelected12:!this.state.isSelected12
    })
  }
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:1}}>
                    <ScrollView>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                    <View>
                                    <Text style={styles.headText}>GENERAL SERVICE</Text>
                                    </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected1}
                                      onValueChange={()=>this.checkboxTest1()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Water wash, Engine oil, Engine oil filter, Starter Clutch
                                        Spark plug, Air cleaner element, Fuel pipe, Chain cleaning and lubrication, 
                                        Engine air breather tube, Silencer cleaning, Break Fluid, Battery electrolyte level,
                                        Wiring and connection routings, Main stand & Side stand pin, General lubrication (Clutch,Brake,Kick) lever,
                                        Coolant level.
                                    </Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>ENGINE OIL</Text>
                                    </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected2}
                                      onValueChange={()=>this.checkboxTest2()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Top Up The Engine</Text>
                                    <Text>Oil Clean Or Replace The Oil Filter</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>AIR FILTER</Text>
                                  </View>
                                   <View> 
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected3}
                                      onValueChange={()=>this.checkboxTest3()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Cleaning And Re-Installing If TheCondition Is Good</Text>
                                    <Text>Changing The Air Filter If The Condition Is Bad</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>DRIVE LINE (CHAIN/BELT)</Text>
                                  </View>
                                    <View> 
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected4}
                                      onValueChange={()=>this.checkboxTest4()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Tighten The Chain/Belt</Text>
                                    <Text>Lube The Chain</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>BRAKE</Text>
                                  </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected5}
                                      onValueChange={()=>this.checkboxTest5()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Replace The Brake Pad Or Shoes If Necessary</Text>
                                    <Text>Adjust Brake Play Or Brake Bleeding</Text>
                                    <Text>Top-Up Brake Fluid</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>CARBURETTOR,THROTTLE AND CHOKE</Text>
                                  </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected6}
                                      onValueChange={()=>this.checkboxTest6()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Adjust The Carburettor Bowl If Necessary</Text>
                                    <Text>Perform Carburettor Adjustment</Text>
                                    <Text>Perform Throttle Cable Adjustment And Ideal Setting</Text>
                                    <Text>Lubricate The Throttle Play</Text>
                                    <Text>Adjust The Choke</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>SPARK PLUG CLEANING/REPLACEMENT</Text>
                                  </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected7}
                                      onValueChange={()=>this.checkboxTest7()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Clean The Spark Plug And Re-Install</Text>
                                    <Text>Replace The Spark Plug</Text>
                                    <Text>Adjust The Spark Plug Terminal</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>ELECTRICAL</Text>
                                  </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected8}
                                      onValueChange={()=>this.checkboxTest8()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Minor Electrical Wiring Adjustment</Text>
                                    <Text>Adjust Headlight Beam</Text>
                                    <Text>Clean Or Adjust Battery Terminal</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>SUSPENSION</Text>
                                  </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected9}
                                      onValueChange={()=>this.checkboxTest9()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Minor Adjustment Of Suspension Play</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>FASTENERS AND LUBRICATION/GREASING</Text>
                                    </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected10}
                                      onValueChange={()=>this.checkboxTest10()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Tighten All The Fasteners</Text>
                                    <Text>Lubricate Side And Centre Stand</Text>
                                    <Text>Lubricate And Adjust Kick And Gear Shifting Lever</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}>CLEANING</Text>
                                    </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected11}
                                      onValueChange={()=>this.checkboxTest11()}
                                    /></View></View>
                                </CardItem>
                                <View style={styles.listTextView}>
                                    <Text>Water wash</Text>
                                    <Text>Dry cleaning</Text>
                                </View>
                            </Card>
                            <Card style={styles.listView}>
                                <CardItem>
                                <View style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={styles.headText}> Others/Not sure?</Text>
                                    </View>
                                    <View>
                                    <RoundCheckbox
                                    backgroundColor="red"
                                      size={24}
                                      checked={this.state.isSelected12}
                                      onValueChange={()=>this.checkboxTest12()}
                                    /></View></View>
                                </CardItem>
                                <TextInput multiline 
                                style={{width:responsiveWidth(90),height:responsiveHeight(13),margin:responsiveWidth(2)}}
                                placeholder="Write your own">

                                </TextInput>
                            </Card>
                                                    
                        </View>
                    </ScrollView>
                </View>
            </View>
            
        )
    }
}

export default Bike_All_Services

const styles=StyleSheet.create({
    listView:{
        width:responsiveWidth(90),
        height:responsiveHeight(25),
        marginTop:responsiveHeight(3),
        backgroundColor:'#EFEFEF'
    },
    headText:{
        fontWeight:'bold'
    },
    listTextView:{
        margin:responsiveWidth(2)
    }
})