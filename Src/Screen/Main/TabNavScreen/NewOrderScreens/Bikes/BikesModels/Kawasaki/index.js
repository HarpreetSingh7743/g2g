let kawasaki = [{value : 'Ninja 300'},
    {value : 'Ninja H2'},
    {value : 'Ninja ZX-10R'},
    {value : 'z1000'},
    {value : 'Ninja 650'},
    {value : 'Z900'},
    {value : 'Ninja 1000'},
    {value : 'Ninja 400'},
    {value : 'Ninja ZX-6R'},
    {value : 'Z650'},
    {value : 'Vulcan S'},
    {value : 'Ninja ZX 14R'},
];

export default kawasaki;