import React, { Component } from 'react'
import { Text, View, TouchableOpacity,StyleSheet } from 'react-native'
import { responsiveWidth, responsiveHeight,responsiveFontSize } from 'react-native-responsive-dimensions'
import { Icon } from "native-base";
import Car_All_services from './Cars/Services/Car_All_services'
import Bike_All_Services from './Bikes/Services/Bike_All_Services'

class SelectServices extends Component {

    state={
        currentScreen:null,
        vehicledata:this.props.navigation.state.params.vehicledata,
    }
    
    renderScreen(){
        this.state.currentScreen=this.state.vehicledata[2]

        if(this.state.currentScreen=="car")
        return(
            <Car_All_services />
        )
        else if(this.state.currentScreen=="bike")
        {
            return(
                <Bike_All_Services />
            )
        }
    }
    render() {
        alert(this.state.vehicledata)
        return (
            <View style={{flex:1}}>
                <View style={{flex:0.15,backgroundColor:'#f11'}}>
                    <View style={{flexDirection:'row',marginTop:responsiveHeight(5),alignItems:'center',justifyContent:'center'}}>
                        <View style={{flexDirection:'row',marginHorizontal:responsiveWidth(3)}}>
                            <View style={{flex:0.9}}>
                                <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}}
                                onPress={()=>{this.props.navigation.goBack()}}>
                                    <Icon name="md-arrow-round-back" type="Ionicons" style={{color:'#fff'}}/>
                                    <Text style={{color:'#fff',marginHorizontal:5,fontSize:responsiveFontSize(2)}}>Back</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:0.1}}><TouchableOpacity onPress={()=>{this.props.navigation.navigate("Cart")}}>
                                   <Icon name="shopping-cart" type="Feather" style={{color:'#fff'}}/>
                               </TouchableOpacity></View>
                        </View>
                    </View>
                </View>
                <View style={{flex:0.85}}>
                    {this.renderScreen()}
                </View>
            </View>
        )
    }
}

export default SelectServices
