import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, TextInput} from 'react-native';
import { Icon, Card, CardItem, Button} from 'native-base';
import {responsiveWidth, responsiveHeight, responsiveFontSize} from 'react-native-responsive-dimensions';
import { Dropdown } from 'react-native-material-dropdown';

let data=[{value:2010},
    {value:2011},
    {value:2012},
    {value:2013},
    {value:2014},
    {value:2015},
    {value:2016},
    {value:2017},
    {value:2018},
    {value:2019},
    {value:2020}
];

var vehicledata = [
    {brand:''},
    {model:''},
    {type:''},
    {reg:''},
    {dom:''},
    {mobilenumber:''}
];
export default class VehicleDetails extends Component{
    state = {
        selected: null,
        regnumber : '',
        phonenumber : '',
        vehicle : this.props.navigation.state.params.vehicle

      }

      checkDetails(){
          let validReg =/(([A-Za-z]){2,3}(|-)(?:[0-9]){1,2}(|-)(?:[A-Za-z]){2}(|-)([0-9]){1,4})|(([A-Za-z]){2,3}(|-)([0-9]){1,4})/
          let vehicleReg=this.state.regnumber
          let IndNum = /^[0]?[6789]\d{9}$/
          let Mobile=this.state.phonenumber
          if(vehicleReg.match(validReg))
          { 
            if(this.state.selected==null)
            {
                alert("Select Model")
            }
            else
            {
                if(Mobile.match(IndNum))
                {
                    this.props.navigation.navigate('SelectServices', {vehicledata:vehicledata})
                }
                else{
                    alert("Please enter Mobile Number")
                }
            }
          }
          else { alert("Please Enter Registration Number in correct Format") }

      }
    render(){
        vehicledata[0] = this.state.vehicle[0];
        vehicledata[1] = this.state.vehicle[1];
        vehicledata[2] = this.state.vehicle[2];
        vehicledata[3] = this.state.regnumber;
        vehicledata[4] = this.state.selected;
        vehicledata[5] = this.state.phonenumber;
        
        return(
            <View style={styles.container}>
               <View style={{flex:0.2,backgroundColor:'#f11'}}>
               <View style={{marginTop:responsiveHeight(6),justifyContent:'center'}}>
                        <View style={{flexDirection:'row',marginHorizontal:responsiveHeight(2)}}>
                           <View style={{flex:0.9}}>
                           <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}}
                           onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon name="md-arrow-round-back" type="Ionicons" style={{color:'#fff'}}/>
                                <Text style={{color:'#fff',marginHorizontal:5,fontSize:responsiveFontSize(2)}}>Back</Text>
                            </TouchableOpacity>
                           </View>
                           <View style={{flex:0.1}}>
                               <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Cart")}}>
                                   <Icon name="shopping-cart" type="Feather" style={{color:'#fff'}}/>
                               </TouchableOpacity>
                           </View>
                            
                        </View>
                    </View>
               </View>
               <View style={{flex:0.8,justifyContent:'center'}}>
                        <View style={{flex:0.1,justifyContent:'center',padding:10}}>
                            <Text style={styles.headerText}>ENTER DETAILS</Text>
                        </View>
                        <View style={{flex:0.9,alignItems:'center'}}>
                        <View style={{height:responsiveHeight(60),width:responsiveWidth(90)}}>
                            <Card style={{flex:1}}>
                                <View style={styles.InputFieldView}>
                                    <View>
                                    <Text style={{marginBottom:responsiveHeight(2)}}>Enter Vehicle Registration Number</Text>
                                    <TextInput style={styles.inputFields}
                                    placeholder="Enter Reg No. (e.g: CH01AB1234)"
                                    onChangeText={(input)=>{this.setState({regnumber : input})}}></TextInput>
                                    </View>
                                </View>
                                <View style={styles.InputFieldView}>
                                <CardItem>
                                    <Dropdown
                                    label='SELECT MODEL'
                                    fontSize={20}
                                    data={data}
                                    containerStyle={styles.container}
                                    fontSize={responsiveFontSize(1.5)}
                                    onChangeText={(input1)=>this.setState({selected : input1})}/>
                                </CardItem>
                                </View>
                                <View style={styles.InputFieldView}>
                                    <View>
                                        <Text style={{marginBottom:responsiveHeight(2)}}>Enter Mobile No.</Text>
                                    <TextInput style={styles.inputFields}
                                    placeholder="Enter Mobile"
                                    onChangeText={(input2)=>{this.setState({phonenumber : input2})}}></TextInput>
                                    </View>
                                </View>
                                <View style={styles.InputFieldView}>
                                        <TouchableOpacity>
                                            <Button style={styles.submitButton}
                                                onPress={()=>{ this.checkDetails()}}>
                                                <Text style={{color:'#fff',fontWeight:'bold'}}>
                                                    Submit
                                                </Text>
                                            </Button>
                                        </TouchableOpacity>
                                </View>
                            </Card>
                        </View>
                    </View>
               </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    headerText:{
        fontSize:responsiveFontSize(3),
        fontWeight:'bold',
        color:'#f11'
    },
    InputFieldView:{
        flex:0.25,
        justifyContent:'center',
        alignItems:'center'
    },
    inputFields:{
        width:responsiveWidth(80),
        borderBottomWidth:1,
        borderBottomColor:'#f11'
    },
    submitButton:{
        width:responsiveWidth(40),
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#f11',
        borderRadius:20
    },
   
})

