import React, { Component } from 'react'
import { Text, View,StyleSheet,Image } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Card, Button } from 'native-base'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { Avatar } from 'react-native-elements'

class Wheel_Care_Services extends Component {
    state={
        rotation_Price:"0.0",
        balancing_Price:"0.0",
        alignment_Price:"0.0"
    }
    render() {
        return (
            <View style={{flex:1}}>
               <ScrollView>
               <View style={{flex:1,margin:responsiveWidth(5),}}>
                    <View style={styles.listView}>
                        <Card style={styles.cardView}>
                            <View style={{flex:0.02,backgroundColor:'#46f'}}></View>
                            <View style={{flex:0.2,borderBottomWidth:0.5}}>
                                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                    <Text style={styles.listHeader}>TYRE ROTATION</Text>
                                </View>
                            </View>
                            <View style={{flex:0.78}}>
                                
                                <View style={{flex:0.8,flexDirection:'row',margin:responsiveWidth(3)}}>
                                    <View style={{margin:responsiveWidth(2),flex:0.5}}>
                                    <Image
                                    source={require('../../../../../../../../assets/Images/Icons/ServiceScreenIcons/Wheel_Care_Services/Wheel-Rotation.png')}
                                    style={{width:responsiveWidth(32),height:responsiveHeight(20)}}/>
                                    </View>
                                    <View style={{flex:0.5,alignItems:'center',justifyContent:'center'}}>
                                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold'}}>
                                            Rs. {this.state.rotation_Price}
                                        </Text>
                                        <TouchableOpacity>
                                            <Button style={{width:responsiveWidth(40),justifyContent:'center',backgroundColor:'#f11',borderRadius:30}}>
                                                <Text style={{color:'#fff'}}>Add To Cart</Text>
                                            </Button>
                                        </TouchableOpacity>
                                    </View>
                                    </View>
                            </View>
                        </Card>
                    </View>

                    <View style={styles.listView}>
                        <Card style={styles.cardView}>
                        <View style={{flex:0.02,backgroundColor:'#f53'}}></View>
                            <View style={{flex:0.2,borderBottomWidth:0.5}}>
                                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                    <Text style={styles.listHeader}>WHEEL ALIGNMENT</Text>
                                </View>
                            </View>
                            <View style={{flex:0.78}}>
                                
                                <View style={{flex:0.8,flexDirection:'row',margin:responsiveWidth(3)}}>
                                    <View style={{margin:responsiveWidth(2),flex:0.5}}>
                                    <Image
                                    source={require('../../../../../../../../assets/Images/Icons/ServiceScreenIcons/Wheel_Care_Services/wheel-alignment.png')}
                                    style={{width:responsiveWidth(40),height:responsiveHeight(18)}}/>
                                    </View>
                                    <View style={{flex:0.5,alignItems:'center',justifyContent:'center'}}>
                                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold'}}>
                                            Rs. {this.state.alignment_Price}
                                        </Text>
                                        <TouchableOpacity>
                                            <Button style={{width:responsiveWidth(40),justifyContent:'center',backgroundColor:'#f11',borderRadius:30}}>
                                                <Text style={{color:'#fff'}}>Add To Cart</Text>
                                            </Button>
                                        </TouchableOpacity>
                                    </View>
                                    </View>
                            </View>
                        </Card>
                    </View>

                    <View  style={styles.listView}>
                        <Card style={styles.cardView}>
                        <View style={{flex:0.02,backgroundColor:'#f28'}}></View>
                            <View style={{flex:0.2,borderBottomWidth:0.5}}>
                                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                    <Text style={styles.listHeader}>WHEEL BALANCING</Text>
                                </View>
                            </View>
                            <View style={{flex:0.78}}>
                                
                                <View style={{flex:0.8,flexDirection:'row',margin:responsiveWidth(3)}}>
                                    <View style={{margin:responsiveWidth(2),flex:0.5}}>
                                    <Image style={{height:responsiveHeight(18),width:responsiveWidth(28)}}
                                    source={require('../../../../../../../../assets/Images/Icons/ServiceScreenIcons/Wheel_Care_Services/Wheel-Balancing.png')}/>
                                    </View>
                                    <View style={{flex:0.5,alignItems:'center',justifyContent:'center'}}>
                                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold'}}>
                                            Rs. {this.state.balancing_Price}
                                        </Text>
                                        <TouchableOpacity>
                                            <Button style={{width:responsiveWidth(40),justifyContent:'center',backgroundColor:'#f11',borderRadius:30}}>
                                                <Text style={{color:'#fff'}}>Add To Cart</Text>
                                            </Button>
                                        </TouchableOpacity>
                                    </View>
                                    </View>
                            </View>
                        </Card>
                    </View>
                </View>
               </ScrollView>
            </View>
        )
    }
}

export default Wheel_Care_Services

const styles=StyleSheet.create({
    listView:{
        marginTop:responsiveHeight(3)
    },
    cardView:{
        width:responsiveWidth(90),
        height:responsiveHeight(30),
        backgroundColor:'#EFEFEF'
    },
    listHeader:{
        fontSize:responsiveFontSize(2)
    }
})