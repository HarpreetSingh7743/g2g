import React, { Component } from 'react'
import { Text, View,StyleSheet, TouchableOpacity } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Card, Button } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'

 class Car_Care_Services_Screen extends Component {
     state={
         teflon_Price:"0.0",
         car_Wash_Price:"0.0",
         anti_rust_Price:"0.0",
         dry_clean_Price:"0.0",
         exterior_price:"0.0",
         comprehensive_Price:"0.0"
     }
    render() {
        return (
            <View style={{flex:1}}>
                <ScrollView>
                <View style={{flex:1,margin:responsiveWidth(5)}}>
                   
                   <View style={styles.listView}>
                        <Card style={{width:responsiveWidth(90),height:responsiveHeight(30),backgroundColor:'#EFEFEF'}}>
                            <View style={{flex:0.2,backgroundColor:'#f11'}}></View>
                            <View style={{flex:3}}>
                                <View style={styles.listHead}>
                                    <Text style={{fontSize:responsiveFontSize(2)}}>TEFLON COATING-BODY</Text>
                                </View>
                            </View>
                            <View style={{flex:6.8,alignItems:'center'}}>
                                <View style={{flex:0.9}}>
                                    <Text>Rs. {this.state.teflon_Price}</Text>
                                </View>
                                <View style={{flex:0.9,alignItems:'center',marginHorizontal:responsiveWidth(5)}}>
                                    <Text>Protects paint from corrosion, adds shine, removes dullness & swirl marks.</Text>
                                </View>
                                <View style={{flex:0.9}}>
                                    <TouchableOpacity>
                                        <Button style={styles.customButton}>
                                            <Text style={{color:'#fff'}}>Add To Cart</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Card>
                    </View>

                    <View style={styles.listView}>
                        <Card style={{width:responsiveWidth(90),height:responsiveHeight(30),backgroundColor:'#EFEFEF'}}>
                        <View style={{flex:0.2,backgroundColor:'#181'}}></View>
                            <View style={{flex:3}}>
                                <View style={styles.listHead}>
                                    <Text style={{fontSize:responsiveFontSize(2)}}>CAR WASH</Text>
                                </View>
                            </View>
                            <View style={{flex:6.8,alignItems:'center'}}>
                            <View style={{flex:0.9}}>
                                    <Text>Rs. {this.state.car_Wash_Price}</Text>
                                </View>
                                <View style={{flex:0.9,alignItems:'center',marginHorizontal:responsiveWidth(5)}}>
                                    <Text>Pressure Car Wash.</Text>
                                </View>
                                <View style={{flex:0.9}}>
                                    <TouchableOpacity>
                                        <Button style={styles.customButton}>
                                            <Text style={{color:'#fff'}}>Add To Cart</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Card>
                    </View>

                    <View style={styles.listView}>
                        <Card style={{width:responsiveWidth(90),height:responsiveHeight(30),backgroundColor:'#EFEFEF'}}>
                        <View style={{flex:0.2,backgroundColor:'#f91'}}></View>
                            <View style={{flex:3}}>
                                <View style={styles.listHead}>
                                    <Text style={{fontSize:responsiveFontSize(2)}}>ANTI-RUST COATING</Text>
                                </View>
                            </View>
                            <View style={{flex:6.8,alignItems:'center'}}>
                            <View style={{flex:0.9}}>
                                    <Text>Rs. {this.state.anti_rust_Price}</Text>
                                </View>
                                <View style={{flex:0.9,alignItems:'center',marginHorizontal:responsiveWidth(5)}}>
                                    <Text>2-3 mm thick rubberized paint coating prevents the car underside from rust.</Text>
                                </View>
                                <View style={{flex:0.9}}>
                                    <TouchableOpacity>
                                        <Button style={styles.customButton}>
                                            <Text style={{color:'#fff'}}>Add To Cart</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Card>
                    </View>

                    <View style={styles.listView}>
                        <Card style={{width:responsiveWidth(90),height:responsiveHeight(40),backgroundColor:'#EFEFEF'}}>
                        <View style={{flex:0.2,backgroundColor:'#14f'}}></View>
                        
                        <View style={{flex:2}}>
                                <View style={styles.listHead}>
                                    <Text style={{fontSize:responsiveFontSize(2)}}>DRY CLEANING</Text>
                                </View>
                        </View>
                        
                        <View style={{flex:7.8,alignItems:'center'}}>
                            <View style={{flex:0.2}}>
                                    <Text>Rs. {this.state.dry_clean_Price}</Text>
                            </View>

                            <View style={{flex:0.2,alignItems:'center',marginHorizontal:responsiveWidth(5)}}>
                                    <Text>Every 3 Months 3M/Wurth Products Used</Text>
                            </View>
                            
                            <View style={{flex:0.2}}>
                                    <TouchableOpacity>
                                        <Button style={styles.customButton}>
                                            <Text style={{color:'#fff'}}>Add To Cart</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            
                            <View style={{flex:0.4,alignItems:'center'}}>
                                    <View><Text>Vaccum Cleaning</Text></View>
                                    <View><Text>Dashboard polishing</Text></View>
                                    <View><Text>Interior Shampooing</Text></View>
                                    <View><Text>Interior Detail Cleaning</Text></View>
                            </View>
                        </View>
                    </Card>
                </View>

                    <View style={styles.listView}>
                    <Card style={{width:responsiveWidth(90),height:responsiveHeight(50),backgroundColor:'#EFEFEF'}}>
                    <View style={{flex:0.2,backgroundColor:'#f1f'}}></View>
                    <View style={{flex:2}}>
                                <View style={styles.listHead}>
                                    <Text style={{fontSize:responsiveFontSize(2)}}>EXTERIOR</Text>
                                    <Text>POLISHING & RUBBING</Text>
                                </View>
                        </View>
                        
                        <View style={{flex:7.8,alignItems:'center'}}>
                            <View style={{flex:0.2}}>
                                    <Text>Rs. {this.state.exterior_price}</Text>
                            </View>

                            <View style={{flex:0.2,alignItems:'center',marginHorizontal:responsiveWidth(5)}}>
                                    <Text>Every 6 Months 3M/Wurth Products Used</Text>
                            </View>
                            
                            <View style={{flex:0.2}}>
                                    <TouchableOpacity>
                                        <Button style={styles.customButton}>
                                            <Text style={{color:'#fff'}}>Add To Cart</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            
                            <View style={{flex:0.4,alignItems:'center'}}>
                                    <View><Text>Pressure Car Wash</Text></View>
                                    <View><Text>Rubbing Compound</Text></View>
                                    <View><Text>Wax polishing</Text></View>
                                    <View><Text>Machine Rubbing</Text></View>
                                    <View><Text>Tyre Dressing & Alloy Cleaning</Text></View>
                            </View>
                        </View>
                        </Card>
                    </View>

                    <View style={styles.listView}>
                        <Card style={{width:responsiveWidth(90),height:responsiveHeight(60),backgroundColor:'#EFEFEF'}}>
                        <View style={{flex:0.2,backgroundColor:'#84f'}}></View>
                        <View style={{flex:1.5}}>
                                <View style={styles.listHead}>
                                    <Text style={{fontSize:responsiveFontSize(2)}}>COMPRENHENSIVE</Text>
                                    <Text>CLEANING</Text>
                                </View>
                        </View>
                        
                        <View style={{flex:8.3,alignItems:'center'}}>
                            <View style={{flex:0.15}}>
                                    <Text>Rs. {this.state.comprehensive_Price}</Text>
                            </View>

                            <View style={{flex:0.2,alignItems:'center',marginHorizontal:responsiveWidth(5)}}>
                                    <Text>A complete package of interior and exterior cleaning services.</Text>
                            </View>
                            
                            <View style={{flex:0.2}}>
                                    <TouchableOpacity>
                                        <Button style={styles.customButton}>
                                            <Text style={{color:'#fff'}}>Add To Cart</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            
                            <View style={{flex:0.5,alignItems:'center'}}>
                                    <View><Text>Vacuum Cleaning</Text></View>
                                    <View><Text>Dashboard Polishing</Text></View>
                                    <View><Text>Interior Shampooing</Text></View>
                                    <View><Text>Pressure Car Wash</Text></View>
                                    <View><Text>Rubbing Compound</Text></View>
                                    <View><Text>Wax Polishing</Text></View>
                                    <View><Text>Machine Rubbing</Text></View>
                                    <View><Text>Tyre Dressing & Alloy Cleaning</Text></View>
                            </View>
                        </View>
                        </Card>
                    </View>
                   
                </View>
                </ScrollView>
            </View>
        )
    }
}

export default Car_Care_Services_Screen

const styles=StyleSheet.create({
    listView:{
        marginTop:responsiveHeight(3)
    },
    listHead:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth:1,
        borderBottomColor:'#999'
    },
    customButton:{
        width:responsiveWidth(50),
        justifyContent:"center",
        backgroundColor:'#f11',
        borderRadius:30
    }
})
