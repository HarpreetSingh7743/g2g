import React from 'react';
import { createAppContainer } from "react-navigation";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import Car_Care_Services_Screen from './Car_Care_Services/Car_Care_Services_Screen'
import Scheduled_Services_Screen from './Scheduled_Services/Scheduled_Services_Screen'
import Wheel_Care_Services from './Wheel_Care_Services/Wheel_Care_Services'


const Car_All_Services= createMaterialTopTabNavigator({
    "Scheduled":Scheduled_Services_Screen,
    "Car Care":Car_Care_Services_Screen,
    "Wheel Care":Wheel_Care_Services
},{
    initialRouteName:'Scheduled',
    tabBarOptions:{
        style:{
            backgroundColor:'#f11'
        },
        activeTintColor:'#fff',
        inactiveTintColor:'#111',
    },
    tabBarPosition:'top',
    swipeEnabled:true,
    lazy:true

});
export default createAppContainer(Car_All_Services)
