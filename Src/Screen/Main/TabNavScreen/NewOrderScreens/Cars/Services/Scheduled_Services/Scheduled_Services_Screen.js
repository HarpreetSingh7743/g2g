import React, { Component } from 'react'
import { Text, View,StyleSheet,ScrollView } from 'react-native'
import { Card, Button } from 'native-base'
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions'
import { TouchableOpacity } from 'react-native-gesture-handler'
export class Scheduled_Services_Screen extends Component {
    state={
        BasicPrice:"0.0",
        StandardPrice:"0.0",
        ComphresivePrice:"0.0",

    }
    render() {
        return (
            <View style={{flex:1}}>
                <ScrollView>
                <View style={styles.container}>
                <View style={{flex:1}}>
                   <View style={{flex:1,margin:responsiveWidth(5)}}>
                   <Card style={{flex:1}}>
                   <View style={styles.customHead}>
                        <View style={{flex:0.7,marginTop:responsiveWidth(2),alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:responsiveFontSize(2.5)}}>BASIC</Text>
                            <Text> Rs. {this.state.BasicPrice}</Text>
                            <View style={{marginTop:responsiveWidth(2),flex:1,width:responsiveWidth(50),alignItems:'center',justifyContent:'center'}}>
                                <Text>Every 5,000 kms / 3 months </Text>
                            </View>
                        </View>
                    
                        <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                            <TouchableOpacity><Button style={{width:responsiveWidth(40),justifyContent:'center',backgroundColor:'#f11',borderRadius:30}}>
                                <Text style={{color:'#fff'}}>Add to Cart</Text></Button></TouchableOpacity>
                        </View>
                   </View>

                   <View style={{flex:0.1,backgroundColor:'#13C212'}}></View>

                    <View style={styles.CustomBody}>
                        <View style={styles.listView}>
                            <Text>Air Filter (CLEANED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Oil Filter (REPLACED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Engine Oil (REPLACED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Wiper Fluid (REPLACED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Battery Water (TOP UP)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Coolant (TOP UP)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Spark Plugs (CLEANED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Car Wash (INCLUDED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Interior Vacuuming (INCLUDED)</Text>
                        </View>
                        
                    </View>
                   </Card>
                   </View>
                </View>
                
            </View>
            <View style={styles.container}>
                <View style={{flex:1}}>
                   <View style={{flex:1,margin:responsiveWidth(5)}}>
                   <Card style={{flex:1}}>
                   <View style={styles.customHead}>
                        <View style={{flex:0.7,marginTop:responsiveWidth(2),alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:responsiveFontSize(2.5)}}>STANDARD</Text>
                            <Text> Rs. {this.state.StandardPrice}</Text>
                            <View style={{marginTop:responsiveWidth(2),flex:1,width:responsiveWidth(50),alignItems:'center',justifyContent:'center'}}>
                                <Text>Every 10,000 kms / 6 months</Text>
                            </View>
                        </View>
                    
                        <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                            <TouchableOpacity><Button style={{width:responsiveWidth(40),justifyContent:'center',backgroundColor:'#f11',borderRadius:30}}>
                                <Text style={{color:'#fff'}}>Add to Cart</Text></Button></TouchableOpacity>
                        </View>
                   </View>

                   <View style={{flex:0.1,backgroundColor:'#135EDC'}}></View>

                    <View style={styles.CustomBody}>
                        <View style={styles.listView}>
                            <Text>INCLUDE BASIC SERVICES</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Oil Filter (REPLACED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>CabinFilter/AC Filter (CLEANED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Break Fluid (CHECKED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Break Pads (SERVICED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Car Wash (INCLUDED)</Text>
                        </View>
                        
                    </View>
                   </Card>
                   </View>
                </View>
                
            </View>
            <View style={styles.container}>
                <View style={{flex:1}}>
                   <View style={{flex:1,margin:responsiveWidth(5)}}>
                   <Card style={{flex:1}}>
                   <View style={styles.customHead}>
                        <View style={{flex:0.7,marginTop:responsiveWidth(2),alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:responsiveFontSize(2.5)}}>COMPHRESIVE</Text>
                            <Text> Rs. {this.state.ComphresivePrice}</Text>
                            <View style={{marginTop:responsiveWidth(2),flex:1,width:responsiveWidth(50),alignItems:'center',justifyContent:'center'}}>
                                <Text>Every 20,000 kms / 12 months </Text>
                            </View>
                        </View>
                    
                        <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                            <TouchableOpacity><Button style={{width:responsiveWidth(40),justifyContent:'center',backgroundColor:'#f11',borderRadius:30}}>
                                <Text style={{color:'#fff'}}>Add to Cart</Text></Button></TouchableOpacity>
                        </View>
                   </View>

                   <View style={{flex:0.1,backgroundColor:'#F08F05'}}></View>

                    <View style={styles.CustomBody}>
                        <View style={styles.listView}>
                            <Text>INCLUDES BASIC + STANDARD SERVICES</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Wheel Alignment (INCLUDED</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Wheel Balancing (INCLUDED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text> Tyre Rotation (INCLUDED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text> Fuel Filter (REPLACED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Gear Oil (FILTERED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Throttle Body Cleaning (CLEANED)</Text>
                        </View>
                        <View style={styles.listView}>
                            <Text>Car Wash (INCLUDED)</Text>
                        </View>
                        
                    </View>
                   </Card>
                   </View>
                </View>
                
            </View>
                </ScrollView>
            </View>
        )
    }
}

export default Scheduled_Services_Screen

const styles=StyleSheet.create({
    container:{
        height:responsiveHeight(100)
    },
    customHead:{
        backgroundColor:'#D7DAD7',
        flex:3,
        
    },
    CustomBody:{
        flex:6.9,
        backgroundColor:"#D7DAD7",
        alignItems:'center'
    },
    listView:{
        flex:0.9,
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth:0.5,
        width:responsiveWidth(80),
        
    }
})