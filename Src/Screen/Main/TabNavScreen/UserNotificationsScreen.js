import React, { Component } from 'react'
import { Text, View ,ImageBackground,StyleSheet, TouchableOpacity} from 'react-native'
import { responsiveWidth,responsiveHeight,responsiveFontSize } from "react-native-responsive-dimensions";
import { Icon, Card,CardItem } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';


 class UserNotificationsScreen extends Component {
    arrayView=[]
    arraycontent=[
        ["Bajaj V15","2018","CH Customs","Started","Service of your Vehicle has been Started"],
        ["Maruti Alto 800","2010","AutoPace","Servicing","Your Vehicle has more problems than you mentioned Your Mechanic wii contact you soon"],
        ["Maruti Alto 800", "2010","AutoPace","Servicing","Your Vehicle is being serviced plz wait"],
        ["Honda City","2012","Honda Care","Repairing","Your Vehicle is completely Repaired as you mentioned"],
        ["Bajaj Domainar","2019","Bajaj Service Station","Serviced","Your Vehicle is serviced plz Contact at Receptopn"]
    ]
    state={
        numberOfNotifications:5
    }
    drawView=()=>{
        this.arrayView=[];
        for(let i=0;i<this.state.numberOfNotifications;i++)
        {
            this.arrayView.push(
                <Card style={styles.notificationView} key ={i}>
                            <CardItem style={{borderTopLeftRadius:20,borderTopRightRadius:20,backgroundColor:'#f11'}}>
                                 <Text style={{color:'#fff',fontSize:responsiveFontSize(1.5),fontWeight:'bold'}}>{i+1}. {this.arraycontent[i][0]}  {this.arraycontent[i][1]}</Text>
                            </CardItem>
                            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                            <View style={{flex:0.5,alignItems:'center',justifyContent:'center'}}>
                                <Text>Garage : {this.arraycontent[i][2]}</Text>
                                <Text>Status : {this.arraycontent[i][3]}</Text>
                            </View>
                            <View style={{flex:0.5,paddingHorizontal:responsiveWidth(1)}}>
                                <Text style={{fontWeight:'bold',color:'#f11'}}>{this.arraycontent[i][4]}</Text>
                            </View>
                            </View>
                </Card>
            )
        }

    }

    render() {
        this.drawView()
        return (
            <View style={{flex:1}}>
                <ImageBackground 
             source={require('../../../../assets/Images/bg.png')} 
             style={{flex:1}}>
                   <View style={{flex:1,margin:responsiveHeight(3)}}>
                        <View style={styles.customHeader}>
                            <View style={{flex:1,flexDirection:'row',marginTop:responsiveHeight(1)}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.openDrawer()}}
                            style={{marginTop:responsiveHeight(1)}}>
                            <Icon name="menu" type="Feather" style={{color:'#fff'}}/>
                            </TouchableOpacity>
                                <Text style={styles.headertxt}>Notifications</Text>
                            </View>
                        </View>
                        
                        <View style={styles.customBody}>
                            <View style={{flex:1}}>
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    {this.arrayView}
                                </ScrollView>
                            </View>
                        </View>
                    </View>
             </ImageBackground>
            </View>
        )
    }
}

export default UserNotificationsScreen

const styles=StyleSheet.create({
    customHeader:{
        flex:0.3,
    },
    customBody:{
        flex:0.7,    
    },

    headertxt:{
        fontSize:responsiveFontSize(3),
        color:'#fff',
        marginLeft:responsiveWidth(2),
        fontWeight:'bold'
    },
    notificationView:{
        width:responsiveWidth(85),
        height:responsiveHeight(25),
        borderWidth:3,
        marginVertical:responsiveHeight(1.5),
        borderRadius:20,
        backgroundColor:'#efefef'
    }
});
