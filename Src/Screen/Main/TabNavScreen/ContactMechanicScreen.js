import React, { Component } from 'react'
import { Text, View ,ImageBackground,StyleSheet,TouchableOpacity,ScrollView, Image} from 'react-native'
import { responsiveWidth,responsiveHeight,responsiveFontSize } from "react-native-responsive-dimensions";
import {Icon,Card,CardItem} from 'native-base'
import call from 'react-native-phone-call';

 class ContactMechanicScreen extends Component {
    arrayView=[]
    numb=""
    arrayContent=[
        ["Bajaj V15","CH Customs","Mukesh Kumar","10 Min","9501205790"],
        ["Honda Amaze","Singh Motors","Sandeep Kumar","31 Min","9876543210"],
        ["Bajaj Dominar","Bajaj Service Center","Santosh Kumar","80 min","9090989887"],
        ["Tata Nexon","Tata Care","Dinesh","91 min","9898865460"]
    ]
    state={
        numberOfNotifications:this.arrayContent.length
    }
    drawView=()=>{
        this.arrayView=[];
        for(let i=0;i<this.state.numberOfNotifications;i++)
        {
            this.arrayView.push(
                <Card style={styles.notificationView} key={i}>
                            <CardItem style={{backgroundColor:'#f11',borderRadius:10}}>
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <View style={{flex:0.6}}>
                                    <Text style={{color:'#fff',fontSize:responsiveFontSize(1.5),fontWeight:'bold'}}>{i+1}. {this.arrayContent[i][0]}</Text>
                                    </View>
                                    <View style={{flex:0.4}}>
                                    <Text style={{color:'#fff'}}>Time Elapsed: {this.arrayContent[i][3]}</Text>
                                    </View>

                                </View>
                                </CardItem>
                            <View style={{flex:0.7,alignItems:'center',padding:responsiveWidth(2),flexDirection:'row'}}>
                            <Image source={require('../../../../assets/Images/Icons/mechanic.png')}
                            style={{width:responsiveWidth(15),height:responsiveHeight(9),borderRadius:50}}/>
                            <View style={{flex:1,padding:responsiveWidth(1.5)}}>
                            <Text>Your vehicle is being serviced in {this.arrayContent[i][1]} by {this.arrayContent[i][2]}</Text>
                            </View>
                            </View>
                            <View  style={{flex:0.3}}>
                                <TouchableOpacity style={{borderWidth:3,width:responsiveWidth(40),
                                                        height:responsiveHeight(4),alignItems:'center',
                                                        justifyContent:'center',borderColor:'#f11',borderRadius:10}}
                                                    onPress={() => {
                                                        this.numb=this.arrayContent[i][4]
                                                        const args = {
                                                          number: this.numb,
                                                          prompt: false,
                                                        };
                                                        call(args).catch(console.error);
                                                      }}>
                                    <Text>Contact</Text>
                                </TouchableOpacity>
                            </View>
                </Card>
            )
        }

    }
    render() {
        this.drawView()
        return (
            
            <View style={{flex:1}}>
                <ImageBackground 
             source={require('../../../../assets/Images/bg.png')} 
             style={{flex:1}}>
                    <View style={{flex:1,margin:responsiveHeight(3)}}>
                        <View style={styles.customHeader}>
                            <View style={{flex:1,flexDirection:'row',marginTop:responsiveHeight(1)}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.openDrawer()}}
                            style={{marginTop:responsiveHeight(1)}}>
                            <Icon name="menu" type="Feather" style={{color:'#fff'}}/>
                            </TouchableOpacity>
                                <Text style={styles.headertxt}>Mechanic</Text>
                            </View>
                        </View>
                        <View style={styles.customBody}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                            {this.arrayView}
                            </ScrollView>
                        </View>
                    </View>
             </ImageBackground>
            </View>
        )
    }
}

export default ContactMechanicScreen

const styles=StyleSheet.create({
    customHeader:{
        flex:0.3,
    },
    customBody:{
        flex:0.7,
    alignItems:'center'   },
        headertxt:{
            fontSize:responsiveFontSize(3),
            color:'#fff',
            marginLeft:responsiveWidth(2),
            fontWeight:'bold'
        },
        notificationView:{
            width:responsiveWidth(85),
            height:responsiveHeight(25),
            borderWidth:3,
            marginVertical:responsiveHeight(1.5),
            borderRadius:10,
            alignItems:'center'
        }
});
