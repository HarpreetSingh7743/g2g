import React, { Component } from 'react'
import { Text, View ,ImageBackground,StyleSheet,TouchableOpacity} from 'react-native'
import { responsiveWidth,responsiveHeight,responsiveFontSize } from "react-native-responsive-dimensions";
import { Card, Icon } from 'native-base';
 class ProfileScreen extends Component {
     state={
         userName:"Harpreet Singh",
         Email:"helloworld@gmail.com",

         phone:"9876543210",
         address:"258/1, sector 11, chandigarh",
         userId:'G2G3245',
         ServicDone:"3"
     }
    render() {
        return (
           
            <View style={{flex:1}}>
                <ImageBackground 
             source={require('../../../../assets/Images/bg.png')} 
             style={{flex:1}}>
                    <View style={{flex:1,margin:responsiveHeight(3)}}>
                        <View style={styles.customHeader}>
                            <View style={{flex:1,flexDirection:'row',marginTop:responsiveHeight(1)}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.openDrawer()}}
                            style={{marginTop:responsiveHeight(1)}}>
                            <Icon name="menu" type="Feather" style={{color:'#fff'}}/>
                            </TouchableOpacity>
                                <Text style={styles.headertxt}>Profile</Text>
                            </View>
                            
                        </View>
                        <View style={styles.customBody}>
                            <Card style={{height:responsiveHeight(70),borderRadius:20}}>
                                <View style={styles.customUpperBody}>
                                    <Text style={{fontSize:responsiveFontSize(3),fontWeight:'bold'}}>{this.state.userName}</Text>
                                    <Text>{this.state.Email}</Text>
                                </View>
                                <View style={styles.customMiddleBody}>
                                    <View style={{margin:10}}>
                                        <View style={styles.innerBodyDetails}>
                                            <Text style={styles.innerBodyText}>Phone : </Text>
                                            <Text>{this.state.phone}</Text>
                                        </View>

                                        <View style={styles.innerBodyDetails}>
                                            <Text style={styles.innerBodyText}>Address : </Text>
                                            <Text>{this.state.address}</Text>
                                        </View>

                                        <View style={styles.innerBodyDetails}>
                                            <Text style={styles.innerBodyText}>UserId : </Text>
                                            <Text>{this.state.userId}</Text>
                                        </View>

                                        <View style={styles.innerBodyDetails}>
                                            <Text style={styles.innerBodyText}>Services Done : </Text>
                                            <Text>{this.state.ServicDone}</Text>
                                        </View>

                                    </View>
                                </View>
                                <View style={styles.customLowerBody}>
                                    <TouchableOpacity style={styles.customButtons}>
                                        <Text style={styles.customButtonText}>Change Password</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.customButtons}>
                                        <Text style={styles.customButtonText}>
                                            LogOut
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </Card> 
                        </View>
                    </View>
             </ImageBackground>
            </View>
        )
    }
}

export default ProfileScreen

const styles=StyleSheet.create({
    customHeader:{
        flex:0.2,
    },
    customBody:{
        flex:0.8,   },
        headertxt:{
            fontSize:responsiveFontSize(3),
            color:'#fff',
            marginLeft:responsiveWidth(2),
            fontWeight:'bold'
        },
        customUpperBody:{
            flex:0.2,
            alignItems:'center',
            justifyContent:'center'
        },
        customMiddleBody:{
            flex:0.6,
            alignItems:'center',
            justifyContent:'center'
        },
        customLowerBody:{
            flex:0.2,
            alignItems:'center',
            justifyContent:'center'
        },
        customButtons:{
            paddingHorizontal:responsiveHeight(3),
            paddingVertical:responsiveHeight(1),
            marginVertical:responsiveHeight(1),
            backgroundColor:'#f11',
            borderRadius:16
        },
        customButtonText:{
            color:'#fff',
        },
        innerBodyDetails:{
            flexDirection:'row',
            marginVertical:10,
            paddingVertical:10
        },
        innerBodyText:{
            fontWeight:'bold'
        }
});
