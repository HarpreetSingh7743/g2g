import React, { Component } from 'react'
import { Text, View,ImageBackground,StyleSheet,TouchableOpacity,Image } from 'react-native'
import { responsiveWidth,responsiveHeight,responsiveFontSize } from "react-native-responsive-dimensions";
import {Icon, Button} from 'native-base'

var selected= null;

 class Dashboard extends Component {
     state={
         activeColor1:'#111',
         activeColor2:'#111',
         activeScreen:""
     }
    render() {
        const { navigate } = this.props.navigation
        selected = this.state.activeScreen;
        return (
            <View style={{flex:1}}>
                <ImageBackground 
                    source={require('../../../../assets/Images/bg.png')} 
                    style={{flex:1}}>
                    <View style={{flex:1,marginTop:responsiveHeight(3)}}>
                        <View style={styles.customHeader}>
                            <View style={{flex:1,marginHorizontal:responsiveWidth(3)}}>
                                <View style={{flexDirection:'row',marginTop:responsiveHeight(3),alignItems:'center'}}>
                                    <View style={{flex:0.9,flexDirection:'row',alignItems:'center'}}>
                                        <TouchableOpacity onPress={()=>{this.props.navigation.openDrawer()}}
                                            style={{marginTop:responsiveHeight(1)}}>
                                            <Icon name="menu" type="Feather" style={{color:'#fff'}}/>
                                        </TouchableOpacity>
                                        <Text style={styles.headertxt}>Home</Text>
                                    </View>
                                    <View style={{flex:0.1,alignItems:'flex-end'}}>
                                        <View>
                                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Cart")}}>
                                                <Icon name="shopping-cart" type="Feather" style={{color:'#fff'}}/>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View>
                                <Text style={{fontSize:responsiveFontSize(4),fontWeight:'bold',color:'#f11'}}>Select Your Vehicle</Text>
                            </View>
                        </View>
                        <View style={styles.customBody}>
                           <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                <View style={{flexDirection:'row',flex:0.8,alignItems:'center'}}>
                                    <TouchableOpacity onPress={()=>{this.setState({"activeColor1":"#f11","activeColor2":"#111","activeScreen":"car"})}}><View style={{ borderWidth:3,
                                                    height:responsiveHeight(22),
                                                    width:responsiveWidth(45),
                                                    alignItems:'center',
                                                    justifyContent:'center',
                                                    borderRadius:100,
                                                    marginHorizontal:10,
                                                    borderColor:this.state.activeColor1}}>
                                        <Image
                                        source={require('../../../../assets/Images/Icons/Vehicles/carlogo.png')}
                                        style={{width:responsiveWidth(35),height:responsiveHeight(13)}}/>
                                    </View></TouchableOpacity>

                                    <TouchableOpacity onPress={()=>{this.setState({"activeColor2":"#f11","activeColor1":"#111","activeScreen":"bike"})}}>
                                    <View style={{ borderWidth:3,
                                                    height:responsiveHeight(22),
                                                    width:responsiveWidth(45),
                                                    alignItems:'center',
                                                    justifyContent:'center',
                                                    borderRadius:100,
                                                    marginHorizontal:10,
                                                    borderColor:this.state.activeColor2}}>
                                        <Image
                                        source={require('../../../../assets/Images/Icons/Vehicles/bikelogo.png')}
                                        style={{width:responsiveWidth(35),height:responsiveHeight(9)}}/>
                                    </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex:0.3}}>
                                    <View style={{flex:1,marginTop:responsiveHeight(5),width:responsiveWidth(90),alignItems:'center',justifyContent:'center'}}>
                                        <TouchableOpacity>
                                            <Button style={styles.submitButton}
                                            onPress={()=>{
                                                if(this.state.activeScreen=="")
                                                {
                                                    alert("Please select atleast one Vehicle !")
                                                }
                                                else{
                                                    navigate('NewOrderAppNav', {selected:selected})
                                                }
                                            }}>
                                                <Text style={{fontSize:responsiveFontSize(3),color:'#fff',fontWeight:'bold'}}>Go</Text>
                                            </Button>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                           </View>
                        </View>
                    </View>
             </ImageBackground>
            </View>
        )
    }
}

export default Dashboard

const styles=StyleSheet.create({
    customHeader:{
            flex:0.35,
        },
    customBody:{
            flex:0.65,
        },
        headertxt:{
            fontSize:responsiveFontSize(3),
            color:'#fff',
            marginLeft:responsiveWidth(2),
            fontWeight:'bold'
        },
        submitButton:{
            width:responsiveWidth(40),
            height:responsiveHeight(10),
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#f11',
            borderRadius:20
        },
});
