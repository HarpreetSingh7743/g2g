import React, { Component } from 'react'
import { View,ActivityIndicator,StyleSheet, Image, ImageBackground,Text, ProgressBarAndroid } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

class SplashScreen extends Component {
   render() {
        return (
            <View style={styles.container}>
               <ImageBackground
               source={require('../../../assets/Images/splash.jpg')}
               style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                  <View style={{flex:1}}>
                        <View style={{flex:0.9,alignItems:'center',justifyContent:'center'}}>
                            <Image source={require('../../../assets/Images/fisso.png')}
                                style={{width:responsiveWidth(50), height:responsiveHeight(25),marginTop:responsiveWidth(60)}}/>
                            </View>
                            
                            <View style={{flex:0.1}}>
                                <ProgressBarAndroid styleAttr="Horizontal" style={{width:responsiveWidth(60)}} color="#f11"/>
                            </View>
                  </View>
               </ImageBackground>
            </View>
        )
    }
}

export default SplashScreen

const styles=StyleSheet.create({
    container:{
        flex:1,
    }
})